
# CAPSTONE PROJECT

# To Run
1. Create Database on localhost name procurement

2. Create virtual environment
    - `python -m venv venv`
    - `venv\scripts\activate`

3. Install dependencies
    - `pip install -r requirements.txt`
    
4. Run server
    - `uvicorn app.main:app --reload`