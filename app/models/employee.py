from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ..database import Base
import uuid




class Employee(Base):
    __tablename__ = "employees"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    first_name = Column(String(255), nullable=False)
    last_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=True)
    birthdate = Column(DATE, nullable=False)
    contact_no = Column(String(255), nullable=False)
    address = Column(String(255), nullable=False)
    status = Column(String(255), nullable=False,default="Active")
    employee_type_id = Column(CHAR(36), ForeignKey("employee_types.id"), nullable=True)
    department_id = Column(CHAR(36), ForeignKey("department.id"), nullable=True)
    created_at = Column(DATETIME, default=func.current_timestamp())
    updated_at = Column(DATETIME,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

    # relation with user
    users = relationship("User", back_populates="employees")
   
     # relation with department
    department = relationship("Department", back_populates="employees")

    # relation with employee types
    employee_types = relationship("EmployeeType", back_populates="employees")



    