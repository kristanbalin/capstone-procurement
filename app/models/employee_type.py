from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ..database import Base, engine
import uuid



class EmployeeType(Base):
    __tablename__ = "employee_types"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    name = Column(String(255), nullable=False)
    description = Column(TEXT, nullable=True)
    status = Column(String(255), nullable=True,default="Active")
    created_at = Column(DATETIME, nullable=True,default=func.current_timestamp())
    updated_at = Column(DATETIME,nullable=True,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

    # relation with employees
    employees = relationship("Employee", back_populates="employee_types")