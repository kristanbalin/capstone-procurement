
import uvicorn,os 
from fastapi import FastAPI

from fastapi.middleware.cors import CORSMiddleware

# import routers
from .routers import (purchase_order_invoice, user, login, user_type,product, category, department,budget_plan, vendor_performance_evaluation,vendor_proposal, vendor_login, terms_of_reference,
purchase_order,purchase_requisition,purchase_requisition_detail,request_quotation,vendor, vendor_bidding_item, notif,project_request,
vendor_log_time,vendor_audit_trail,related_documents, utility,rfq_vendor,employee,employee_type,payment_method,payment_terms,returns,return_details,replacement_request)

from fastapi.staticfiles import StaticFiles

from .database import engine, Base

# import web pages routers
from .pages_routers.procurement.p_manager_pages import p_manager_user_pages
from .pages_routers.procurement.p_officer_pages import p_officer_user_pages
from .pages_routers.procurement.dept_user_pages import dept_user_pages
from .pages_routers.procurement.vendor_pages import vendor_user_pages
from .pages_routers.procurement.admin import admin_user_pages
from .pages_routers.general.general import general_user_pages




app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

Base.metadata.create_all(engine)

# routers
app.include_router(employee.router)
app.include_router(employee_type.router)

app.include_router(user.router)
app.include_router(user_type.router)
app.include_router(login.router)
app.include_router(product.router)
app.include_router(category.router)
app.include_router(department.router)
app.include_router(budget_plan.router)
app.include_router(purchase_order.router)
app.include_router(purchase_requisition.router)
app.include_router(purchase_requisition_detail.router)
app.include_router(request_quotation.router)
# app.include_router(vendor_evaluation_schedules.router)
app.include_router(vendor_performance_evaluation.router)
app.include_router(vendor_proposal.router)
app.include_router(vendor_bidding_item.router)
app.include_router(vendor.router)
app.include_router(vendor_login.router)
app.include_router(terms_of_reference.router)
app.include_router(notif.router)
app.include_router(vendor_log_time.router)
app.include_router(vendor_audit_trail.router)
app.include_router(related_documents.router)

app.include_router(purchase_order_invoice.router)
app.include_router(utility.router)
app.include_router(rfq_vendor.router)
app.include_router(payment_terms.router)
app.include_router(payment_method.router)
app.include_router(project_request.router)
app.include_router(returns.router)
app.include_router(return_details.router)
app.include_router(replacement_request.router)











# web pages
app.include_router(p_manager_user_pages)
app.include_router(p_officer_user_pages)
app.include_router(dept_user_pages)
app.include_router(vendor_user_pages)
app.include_router(admin_user_pages)
app.include_router(general_user_pages)


app.mount('/static', StaticFiles(directory="static"), name="static")



if __name__ == '__main__':
    # uvicorn.run(app, host='127.0.0.1', port=8000, workers=2, debug=True)
    uvicorn.run(app)


