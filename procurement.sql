-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2022 at 08:19 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `procurement`
--

-- --------------------------------------------------------

--
-- Table structure for table `budget_plan`
--

CREATE TABLE `budget_plan` (
  `id` varchar(255) NOT NULL,
  `given_budget` float NOT NULL,
  `total_spent` float NOT NULL,
  `year` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `budget_plan`
--

INSERT INTO `budget_plan` (`id`, `given_budget`, `total_spent`, `year`, `date_from`, `date_to`, `status`, `department_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('2ff1321a-82c0-4c95-b6ad-841f4abddd42', 300000, 155912, '2022', '2022-01-01', '2022-12-31', 'active', '9d8afef8-326b-4a3f-a256-9c3510b582bf', NULL, NULL, '2022-02-26 00:59:56', '2022-02-26 14:37:05'),
('5080d02c-8411-4320-82b6-4004c5a5395d', 1000000, 0, '2022', '2022-01-01', '2022-12-31', 'active', 'c80c8b9b-846e-4bc6-a024-b8079250cdb7', NULL, NULL, '2022-02-26 00:56:28', '2022-02-26 00:56:28'),
('da180498-d5a0-4743-b506-a29cdbb390ec', 200000, 0, '2022', '2022-01-01', '2022-12-31', 'active', 'f066642a-70fe-441f-a7f1-6ef2d36a4cf6', NULL, NULL, '2022-02-26 01:06:10', '2022-02-26 01:06:10');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('26a9a648-c08e-4117-9d01-7366ca8943d8', 'IT supplies', 'Any services, Software, Hardware or Third Party Support which we have agreed to supply to you in an Order and/or Statement of Work', 'Active', NULL, NULL, '2021-11-26 17:48:33', '2022-02-25 23:20:22'),
('2f4fcd58-e1ee-479e-89b0-51fcd384c71e', 'Water Services', 'water and wastewater services provider of cities and municipalities', 'active', NULL, NULL, '2022-02-26 11:13:34', '2022-02-26 11:13:34'),
('3b50adf9-4040-491c-84da-0b4ae169a6cb', 'Construction', 'Construction products include products such as doors, windows, shutters and gates, membranes, thermal insulation products, chimneys and flues, sanitary appliances, fire alarms, flooring, fire retardant products, space heating appliances, power cables, glass, and fixings.', 'Active', NULL, NULL, '2021-11-25 12:52:31', '2022-02-25 23:19:35'),
('43dc422a-8621-4966-8662-f8d53d5fdfc7', 'Transport ', 'test', 'Active', NULL, NULL, '2021-11-12 15:56:51', '2022-01-05 15:49:39'),
('7479b7bf-2ae2-4815-bb80-7d9ba73820d7', 'Laboratory', 'Medical laboratory equipment  is any technological system or device used to diagnose, treat, prevent or rehabilitate.', 'active', NULL, NULL, '2021-11-12 15:56:27', '2022-02-25 23:22:46'),
('bdbef0d7-4e29-4d1d-989f-e395941fec5c', 'Office Supplies', 'A consumable item/product used regularly in an office environment to perform departmental personnel\'s daily work assignments.', 'Active', NULL, NULL, '2021-11-12 15:56:40', '2022-02-25 23:20:59'),
('cd434944-9ca7-4858-8c55-675ec1d40570', 'Electricity', 'Electric description', 'active', NULL, NULL, '2022-02-26 11:14:20', '2022-02-26 11:14:20'),
('d86dc4f6-10d1-4a94-b422-03d0a5310c3b', 'Medical Equipment', 'The devices or machines that are used in medical surgeries called as Medical Equipments. ', 'Active', NULL, NULL, '2021-11-12 15:56:18', '2022-02-25 23:18:07'),
('f55f47b2-3247-48e8-805c-731694b9d342', 'Dental Supplies and Equipment', 'Consumable Dental Products used by dentists and dental equipment', 'active', NULL, NULL, '2022-02-25 23:25:15', '2022-02-25 23:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` varchar(255) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `department_head` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department_name`, `department_head`, `contact_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('9d8afef8-326b-4a3f-a256-9c3510b582bf', 'Human Resource', 'PE', '0123123', NULL, NULL, '2022-02-17 22:03:25', '2022-02-17 22:03:25'),
('c80c8b9b-846e-4bc6-a024-b8079250cdb7', 'Procurement', 'Kathniel', '091234123', NULL, NULL, '2022-02-17 22:13:33', '2022-02-26 01:05:44'),
('f066642a-70fe-441f-a7f1-6ef2d36a4cf6', 'Finance', 'J', '0123123', NULL, NULL, '2022-02-17 22:03:51', '2022-02-17 22:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `employee_type_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birthdate`, `contact_no`, `address`, `status`, `department_id`, `employee_type_id`, `created_at`, `updated_at`) VALUES
('0d82e69b-178e-4151-90f0-319c01aaed9f', 'Danielle', 'Dadoy', '', '2022-02-26', 'string', 'string', 'Active', 'c80c8b9b-846e-4bc6-a024-b8079250cdb7', '55c6d032-d27d-4cf7-9bb4-62d6c1543169', '2022-02-17 22:14:02', '2022-02-17 22:14:02'),
('1bebbc91-4be4-46af-85ae-7b893d5ce271', 'Aime', 'Arsolon', '', '2022-02-26', 'string', 'string', 'Active', 'c80c8b9b-846e-4bc6-a024-b8079250cdb7', '6df319f6-9695-4d46-8cab-c04ddf2fb8f9', '2022-02-17 22:17:03', '2022-02-17 22:17:03'),
('98a7e93d-aa13-465f-8cd2-5bc3adbe06dc', 'kians', 'balin', 'diz', '2022-03-05', '09123412932', 'wew', 'Active', '9d8afef8-326b-4a3f-a256-9c3510b582bf', '5ce556e7-91b2-4e2f-a95e-31b06106e7c1', '2022-02-24 11:38:14', '2022-02-24 11:38:34'),
('99e265a7-1049-4b4b-abe4-89b7caca3a95', 'admin', 'string', 'string', '2022-02-26', 'string', 'string', 'Active', 'c80c8b9b-846e-4bc6-a024-b8079250cdb7', 'cfa17a36-aa60-4f18-95d7-ea3fd7558410', '2022-02-18 11:06:54', '2022-02-18 11:06:54'),
('c54ee7cc-8dd2-4ec8-a885-496fbec04949', 'tets', 'test', 'test', '2022-02-26', '008123', 'test', 'Active', '9d8afef8-326b-4a3f-a256-9c3510b582bf', '5ce556e7-91b2-4e2f-a95e-31b06106e7c1', '2022-02-24 11:34:23', '2022-02-24 11:34:23'),
('ff8fd782-3331-43f3-a2e2-bf925c3eb3c5', 'Justine', 'Pe', '', '2022-02-26', 'string', 'string', 'Active', '9d8afef8-326b-4a3f-a256-9c3510b582bf', '5ce556e7-91b2-4e2f-a95e-31b06106e7c1', '2022-02-17 22:11:38', '2022-02-17 22:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `employee_types`
--

CREATE TABLE `employee_types` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_types`
--

INSERT INTO `employee_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('55c6d032-d27d-4cf7-9bb4-62d6c1543169', 'Procurement Manager', 'string', 'Active', '2022-02-17 22:12:08', '2022-02-17 22:12:08'),
('5ce556e7-91b2-4e2f-a95e-31b06106e7c1', 'HR Manager', 'Human Resource', 'Active', '2022-02-17 22:01:46', '2022-02-22 10:40:59'),
('6df319f6-9695-4d46-8cab-c04ddf2fb8f9', 'Procurement Officer', 'Procurement', 'Active', '2022-02-17 21:59:20', '2022-02-17 21:59:20'),
('cfa17a36-aa60-4f18-95d7-ea3fd7558410', 'admin', 'string', 'Active', '2022-02-18 11:06:27', '2022-02-18 11:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` varchar(255) NOT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `amount_paid` varchar(255) DEFAULT NULL,
  `purchase_order_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `notif_to` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `notif_to`, `description`, `status`, `vendor_id`, `created_at`, `updated_at`) VALUES
('150ebcf7-71ed-43cb-bd66-b35af22842b9', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('a44634e1-88bc-480f-8d9d-aab7519cf821', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('aec1f0ac-a356-491f-987f-6af8dafea046', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('b2982944-ce06-4d5d-8b81-7c18937007d2', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:26:14', '2022-02-26 14:26:14'),
('dacd0a26-1856-41b3-a64b-62245c2ae898', 'NEW RFQ', 'vendor', NULL, 'unread', NULL, '2022-02-26 15:18:54', '2022-02-26 15:18:54'),
('e176c12c-f599-4542-9427-c0f314ae4c15', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:25:37', '2022-02-26 14:25:37'),
('e3f47c52-6259-4a0d-8d9a-c5bae31110af', 'Purchase Request', 'p_dept_users', 'Your request has beenApproved', 'unread', NULL, '2022-02-26 14:37:05', '2022-02-26 14:37:05'),
('fdb04d68-e117-49fd-857d-798e42ff430b', 'New pr', 'procurement_manager', 'New purchase request', 'unread', NULL, '2022-02-26 14:26:31', '2022-02-26 14:26:31');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` varchar(255) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `method_name`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('49d8f54f-9ff7-4ee8-80ed-2674c28bd9e2', 'Cheque', 'string', 'Active', NULL, NULL, '2022-02-18 21:18:45', '2022-02-18 21:18:45'),
('76ef42d0-1538-480b-a66f-7d823d7541f4', 'Cash', 'string', 'Active', NULL, NULL, '2022-02-18 21:18:29', '2022-02-18 21:18:29'),
('77c69f0d-02d4-4b26-a58c-aba822665353', 'Gcash', 'string', 'Active', NULL, NULL, '2022-02-18 21:18:36', '2022-02-18 21:18:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_terms`
--

CREATE TABLE `payment_terms` (
  `id` varchar(255) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_terms`
--

INSERT INTO `payment_terms` (`id`, `method_name`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('2328267c-a024-4839-b07e-60edf293ac61', 'Partial Payment', 'string', 'Active', NULL, NULL, '2022-02-18 21:18:11', '2022-02-18 21:18:11'),
('985fe432-d814-40dc-add9-62695e3f6e8a', 'Full Payment', 'string', 'Active', NULL, NULL, '2022-02-18 21:18:19', '2022-02-18 21:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `estimated_price` float DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `description`, `estimated_price`, `created_by`, `updated_by`, `status`, `category_id`, `created_at`, `updated_at`) VALUES
('045b2af4-44a5-4ab1-a18f-a8cc1b18ec45', 'product 1', '<ul><li>test only</li><li>test only</li></ul>', 2000, NULL, NULL, 'Inactive', '3b50adf9-4040-491c-84da-0b4ae169a6cb', '2022-01-02 20:48:33', '2022-01-29 13:06:45'),
('08a55159-dd55-4b67-91a1-a234c9ef95fc', 'Sterilizers', 'A machine that uses steam under pressure to kill harmful bacteria&nbsp;&nbsp;viruses&nbsp;fungi&nbsp;&nbsp;and spores on items that are placed inside a pressure vessel', 3000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:29:27', '2022-02-25 23:29:27'),
('1203460d-677e-4fcc-9663-c943ca504bd9', 'cartss', 'wads', 22222, NULL, NULL, 'active', '3b50adf9-4040-491c-84da-0b4ae169a6cb', '2022-01-14 15:57:19', '2022-01-14 15:57:19'),
('12f5ab75-fd0b-4056-8469-78d7d89ac61e', 'Dental Unit Chair', 'designed medical device intended to support a patient’s body when a dental procedure is being performed on them. The electrically operational chair comes with a water line&nbsp;&nbsp;micromotor compressed air and is retractable as per the dentist’s requirements during a procedure. The equipment comes with a seat where the dentist sits while working on the patient', 43000, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:47:44', '2022-02-25 23:47:44'),
('1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', 'Manila folders', 'Filing Cabinet', 230, NULL, NULL, 'active', 'bdbef0d7-4e29-4d1d-989f-e395941fec5c', '2022-02-25 23:52:09', '2022-02-25 23:52:09'),
('2286180d-8eb4-4567-bc4b-ddb88213e68c', 'cart', 'test', 2000, NULL, NULL, 'active', '26a9a648-c08e-4117-9d01-7366ca8943d8', '2022-01-14 15:27:14', '2022-01-14 15:27:14'),
('26b73892-6da0-4532-bb4c-84951650cf97', 'Patient blood gas analyzers', 'Blood gas/pH analyzersuse electrodes to determine pH&nbsp;&nbsp;partial pressure of carbon dioxide&nbsp;and partial pressure of oxygen in the blood', 24950, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:53:37', '2022-02-25 23:53:37'),
('3e52e653-4aa8-43f7-a359-9c06d5f41483', 'prod2', 'test', 2000, NULL, NULL, 'active', '3b50adf9-4040-491c-84da-0b4ae169a6cb', '2022-01-14 15:27:25', '2022-01-14 15:27:25'),
('414d5d7e-80f3-4154-8ed2-d2ddef3e25ae', 'Hospital Stretchers', 'The stretcher is made a hard aluminum&nbsp;that is also light weight and durable. The weight capacity of the product can support patients with a weight of 440 pounds', 10000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:41:22', '2022-02-25 23:41:22'),
('477b3e99-1006-4687-b038-8d09e1ceab0f', 'ECG Machine', 'test', 123, NULL, NULL, 'active', '3b50adf9-4040-491c-84da-0b4ae169a6cb', '2022-01-02 12:25:35', '2022-02-18 23:06:52'),
('48d22463-2900-4f85-83a5-ca581beb4bd9', 'Dental Air Compressor', 'A dental air compressor is a specially designed compressor aimed for a dental or medical practice', 4750, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:34:02', '2022-02-25 23:34:02'),
('494f4e92-2572-4e67-9954-8ea142f35526', 'Anesthesia Machines', 'An anesthesia machine is the&nbsp;apparatus used to deliver general anesthesia to patients as they undergo a medical procedure', 200000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:35:59', '2022-02-25 23:35:59'),
('70a874ed-221a-42cf-aec4-8ac9dc7398d3', 'Denture Box Storage', 'Denture storage boxes make an excellent container for patients with dentures or other dental work such as an athletic mouthguard.', 380, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:40:04', '2022-02-25 23:40:04'),
('762efc7d-25c6-4e83-a625-e001ef17b770', 'Defibrillators', 'Automated external defibrillators (AEDs) are portable&nbsp;life-saving devices designed to treat people experiencing sudden cardiac arrest&nbsp;a medical condition in which the heart stops beating suddenly and unexpectedly', 50000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:50:22', '2022-02-25 23:50:22'),
('810444f4-6461-4cc3-9541-a8b457839756', 'Patient Monitors', 'A patient monitor is an electronic medical device that consists of one of more monitoring sensors&nbsp; and a screen display (also called a \"\"monitor\"\") that provide and record for medical professionals a patient\'s medical vital signs', 180000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:39:00', '2022-02-25 23:39:00'),
('8b031b24-e14c-4d99-bd29-e4be84b0adb9', 'Tongue Cleaner', 'Designed by a dentist to reach far back on the tongue without gagging', 1149, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:48:51', '2022-02-25 23:48:51'),
('92fcdbe9-4b91-4587-9715-3eea8de84edc', 'Tooth and denture brushes', 'used for cleaning and polishing teeth replacements', 79, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:51:26', '2022-02-25 23:51:26'),
('978402d5-4a7a-4122-a685-c1579b343273', 'Surgical and Exam Lights', 'provide lighting in surgical suites&nbsp;&nbsp;low-contrast objects at varying depths in incisions and body cavities', 5000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:55:27', '2022-02-25 23:55:27'),
('9f986ed7-25b6-4f25-b906-9732a51673e8', 'Zinc F+ (Phosphate Cement)', 'Zinc F + is a zinc phosphate cement suitable for the cementation of crowns&nbsp;bridges inlays facings and orthodontic brackets', 700, NULL, NULL, 'active', 'f55f47b2-3247-48e8-805c-731694b9d342', '2022-02-25 23:37:46', '2022-02-25 23:37:46'),
('a0dd63a5-6d92-464e-a3e4-01169c332ca6', 'prod2', 'test', 123, NULL, NULL, 'Inactive', '3b50adf9-4040-491c-84da-0b4ae169a6cb', '2022-01-14 15:27:25', '2022-01-14 15:27:31'),
('adff82ad-2939-4f62-8713-6114d7734b43', 'Patient Gown', 'A short collarless gown that ties in the back worn by patients being examined or treated in a doctor\'s office&nbsp;&nbsp;clinic&nbsp;&nbsp;or hospital', 250, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:46:07', '2022-02-25 23:46:07'),
('c2793a6b-27c9-410e-a624-c923847c6ac8', 'Blanket and Fluid Warmers', 'Blankets are gently heated&nbsp;and assist thermo-regulation. Blanket warming cabinets are recommended&nbsp;for Recovery&nbsp;&nbsp;Maternity&nbsp;&nbsp;General Ward&nbsp;Specialist and Intensive Care Units', 27000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:32:47', '2022-02-25 23:32:47'),
('c8954463-0642-4224-82d0-b0bb94c8e45f', 'Desktop Computers', 'personal computing device designed to fit on top of a typical office desk', 15830, NULL, NULL, 'active', '26a9a648-c08e-4117-9d01-7366ca8943d8', '2022-02-25 23:54:23', '2022-02-25 23:54:23'),
('cb79b5b5-6357-44af-afff-cd771c79833e', 'Medical specimen centrifuges', 'A centrifuge is a device used to separate mixtures made up of parts with different densities.', 50000, NULL, NULL, 'active', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', '2022-02-25 23:44:34', '2022-02-25 23:44:34'),
('d5d12aee-2119-4b30-8663-b9d3368b9fb7', 'Headsets', 'a hardware device that connects to a telephone or computer&nbsp;allowing the user to talk and listen while keeping their hands free', 5000, NULL, NULL, 'active', '26a9a648-c08e-4117-9d01-7366ca8943d8', '2022-02-25 23:42:35', '2022-02-25 23:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `project_request`
--

CREATE TABLE `project_request` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `background` text DEFAULT NULL,
  `coverage` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `target_beneficiaries` varchar(255) DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `expected_output` text DEFAULT NULL,
  `assumptions` varchar(255) DEFAULT NULL,
  `constraints` varchar(255) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `approval_status` varchar(255) DEFAULT NULL,
  `active_status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_request`
--

INSERT INTO `project_request` (`id`, `name`, `background`, `coverage`, `type`, `target_beneficiaries`, `objectives`, `expected_output`, `assumptions`, `constraints`, `cost`, `start_date`, `end_date`, `approval_status`, `active_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('2bed7618-099a-4a92-b2a1-6532830e8f5b', 'TITLE OF THE PROJECT 5\n', 'Background or Project Background is one of the key characteristics of a project to explain\nwhy initiate the project, what prerequisites are, and what results are supposed to be obtained\nat the successful completion.\n', 'string', 'string', 'string', 'A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.\n', 'string', 'string', 'string', 0, '2022-02-26', '2022-03-30', 'Approved', 'Active', NULL, NULL, '2022-02-26 13:04:02', '2022-02-26 13:04:02'),
('3d6c977c-1f7b-4fa5-9756-4a913e5ffa5c', 'TITLE OF THE PROJECT 3\n', 'Background or Project Background is one of the key characteristics of a project to explain\nwhy initiate the project, what prerequisites are, and what results are supposed to be obtained\nat the successful completion.\n', 'string', 'string', 'string', 'A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.\n', 'string', 'string', 'string', 0, '2022-02-26', '2022-03-17', 'Approved', 'Active', NULL, NULL, '2022-02-26 12:59:33', '2022-02-26 12:59:33'),
('abc5b9ac-ddea-4e90-800a-2595da4330a3', 'TITLE OF THE PROJECT 4\n', 'Background or Project Background is one of the key characteristics of a project to explain\nwhy initiate the project, what prerequisites are, and what results are supposed to be obtained\nat the successful completion.\n', 'string', 'string', 'string', 'A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.\n', 'string', 'string', 'string', 0, '2022-02-26', '2022-02-26', 'Approved', 'Active', NULL, NULL, '2022-02-26 13:04:00', '2022-02-26 13:04:00'),
('bee20a70-a5c6-4c34-8655-5a4b73fcc42a', 'TITLE OF THE PROJECT 2\n', 'Background or Project Background is one of the key characteristics of a project to explain\nwhy initiate the project, what prerequisites are, and what results are supposed to be obtained\nat the successful completion.\n', 'string', 'string', 'string', 'A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.\n', 'string', 'string', 'string', 0, '2022-02-26', '2022-02-26', 'Approved', 'Active', NULL, NULL, '2022-02-26 12:59:36', '2022-02-26 12:59:36'),
('f8eee60b-0ef0-42de-82cd-5c63b047c64f', 'TITLE OF THE PROJECT 1\n', 'Background or Project Background is one of the key characteristics of a project to explain\nwhy initiate the project, what prerequisites are, and what results are supposed to be obtained\nat the successful completion.\n', 'string', 'string', 'string', 'A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.\n', 'string', 'string', 'string', 0, '2022-02-26', '2022-03-31', 'Approved', 'Active', NULL, NULL, '2022-02-26 12:56:36', '2022-02-26 12:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` varchar(255) NOT NULL,
  `purchase_order_number` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `expected_delivery_date` date DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `subtotal` float NOT NULL,
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `total_amount` float NOT NULL,
  `shipping_method` varchar(255) DEFAULT NULL,
  `payment_terms_id` varchar(255) DEFAULT NULL,
  `payment_method_id` varchar(255) DEFAULT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `vendor_proposal_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_detail`
--

CREATE TABLE `purchase_order_detail` (
  `id` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `purchase_order_id` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_detail`
--

INSERT INTO `purchase_order_detail` (`id`, `product_name`, `quantity`, `category`, `product_price`, `status`, `purchase_order_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('84f91729-f1db-4f8d-b52d-04c1da6b9ca8', 'test', 122, 'IT supplies', 1222, 'active', '62edd307-9e3c-4678-86cf-6c588e1f10fa', NULL, NULL, '2022-02-26 03:51:08', '2022-02-26 03:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requisition`
--

CREATE TABLE `purchase_requisition` (
  `id` varchar(255) NOT NULL,
  `purchase_requisition_number` int(11) DEFAULT NULL,
  `purpose` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_approved` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `given_budget` float DEFAULT NULL,
  `estimated_amount` float DEFAULT NULL,
  `has_quotation` tinyint(1) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_requisition`
--

INSERT INTO `purchase_requisition` (`id`, `purchase_requisition_number`, `purpose`, `message`, `status`, `date_approved`, `approved_by`, `given_budget`, `estimated_amount`, `has_quotation`, `reason`, `department_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('063e3152-f78a-4715-b572-79164d0aad76', 3795, 'For Ware House Restocking', 'Apart from the system specification as have mentioned above, the technical\nspecification for ICT Equipment will be the hardware requirements which will be used\nin developing the system&nbsp;', 'Draft', NULL, NULL, NULL, 3000, 0, NULL, '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:26:14', '2022-02-26 14:26:14'),
('46f7dad7-126f-4929-ac6f-157645df774c', 5829, 'For Ware House Restocking', 'Apart from the system specification as have mentioned above, the technical\nspecification for ICT Equipment will be the hardware requirements which will be used\nin developing the system&nbsp;', 'Pending', NULL, NULL, NULL, 70452, 0, NULL, '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('4e9e2e27-af40-4fd7-b16d-442aac1557cd', 1127, 'For Ware House Restocking', 'The technical\nspecification for Equipment will be the hardware requirements which will be used\nin developing the system\n', 'Approved', NULL, 'Danielle Dadoy', 155912, 115912, 0, '', '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:25:37', '2022-02-26 14:37:05'),
('5a9d3b36-505b-4f80-8eec-baf1c4f4defc', 1463, 'For Ware House Restocking', 'Apart from the system specification as have mentioned above, the technical\nspecification for ICT Equipment will be the hardware requirements which will be used\nin developing the system&nbsp;', 'Pending', NULL, NULL, NULL, 198142, 0, NULL, '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('c9fe9d07-5638-4145-9d6b-d5638d125a91', 6658, 'For Ware House Restocking', 'Apart from the system specification as have mentioned above, the technical\nspecification for ICT Equipment will be the hardware requirements which will be used\nin developing the system&nbsp;', 'Pending', NULL, NULL, NULL, 68452, 0, NULL, '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('e4f8bfc3-c118-47b0-8774-21b1c215fdc3', 6879, 'For Ware House Restocking', 'Apart from the system specification as have mentioned above, the technical\nspecification for ICT Equipment will be the hardware requirements which will be used\nin developing the system&nbsp;', 'Pending', NULL, NULL, NULL, 127356, 0, NULL, '9d8afef8-326b-4a3f-a256-9c3510b582bf', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, '2022-02-26 14:26:02', '2022-02-26 14:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requisition_detail`
--

CREATE TABLE `purchase_requisition_detail` (
  `id` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `new_category` varchar(255) DEFAULT NULL,
  `new_product_name` varchar(255) DEFAULT NULL,
  `estimated_price` float DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `purchase_requisition_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_requisition_detail`
--

INSERT INTO `purchase_requisition_detail` (`id`, `quantity`, `description`, `new_category`, `new_product_name`, `estimated_price`, `product_id`, `purchase_requisition_id`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`) VALUES
('022fcd53-56ea-41d9-87a2-9bc3fa247627', 3, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', 'e4f8bfc3-c118-47b0-8774-21b1c215fdc3', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('0bba0b5c-9e8a-4b5b-a681-ca1d9cb4e097', 4, NULL, NULL, NULL, NULL, '2286180d-8eb4-4567-bc4b-ddb88213e68c', 'e4f8bfc3-c118-47b0-8774-21b1c215fdc3', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('12bba262-2d0f-45ba-b2bf-b4eeb1a192ff', 1, NULL, NULL, NULL, NULL, '1203460d-677e-4fcc-9663-c943ca504bd9', '5a9d3b36-505b-4f80-8eec-baf1c4f4defc', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('22137827-9f74-44de-90dd-4696cc373306', 1, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', '46f7dad7-126f-4929-ac6f-157645df774c', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('31d87754-eaa5-48a2-8081-314c97a2442d', 4, NULL, NULL, NULL, NULL, '12f5ab75-fd0b-4056-8469-78d7d89ac61e', '5a9d3b36-505b-4f80-8eec-baf1c4f4defc', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('339a9700-113b-4cf8-a0e7-87af8ec3016c', 1, NULL, NULL, NULL, NULL, '12f5ab75-fd0b-4056-8469-78d7d89ac61e', 'e4f8bfc3-c118-47b0-8774-21b1c215fdc3', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('79340b97-9047-4d07-a067-744cfd48acfb', 1, NULL, NULL, NULL, NULL, '1203460d-677e-4fcc-9663-c943ca504bd9', '46f7dad7-126f-4929-ac6f-157645df774c', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('7a79a286-f788-452b-b1c0-da791b356962', 1, NULL, NULL, NULL, NULL, '1203460d-677e-4fcc-9663-c943ca504bd9', 'c9fe9d07-5638-4145-9d6b-d5638d125a91', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('7ca2442d-3528-4e1d-9740-053b4921268d', 1, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', '063e3152-f78a-4715-b572-79164d0aad76', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:14', '2022-02-26 14:26:14'),
('7e179963-3bc6-4022-9ded-b69ee8cc6e8a', 1, NULL, NULL, NULL, NULL, '1203460d-677e-4fcc-9663-c943ca504bd9', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:25:37', '2022-02-26 14:25:37'),
('7f33a6e9-afb9-4820-a328-d3c1f61428ca', 3, NULL, NULL, NULL, NULL, '1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', 'e4f8bfc3-c118-47b0-8774-21b1c215fdc3', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('8772c6e5-18e2-48c3-abec-7bcda45dc233', 1, NULL, NULL, NULL, NULL, '12f5ab75-fd0b-4056-8469-78d7d89ac61e', 'c9fe9d07-5638-4145-9d6b-d5638d125a91', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('971b9173-45f1-407e-bfb9-2de69181ada6', 1, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', 'c9fe9d07-5638-4145-9d6b-d5638d125a91', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('a54bdaf9-be97-4448-8591-7181c36e4e2d', 3, NULL, NULL, NULL, NULL, '1203460d-677e-4fcc-9663-c943ca504bd9', 'e4f8bfc3-c118-47b0-8774-21b1c215fdc3', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:02', '2022-02-26 14:26:02'),
('b65a9ad1-de4e-452e-b5fc-9769587b192d', 1, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:25:37', '2022-02-26 14:25:37'),
('b8920812-0e62-44e1-9278-a4fb670edf57', 2, NULL, NULL, NULL, NULL, '12f5ab75-fd0b-4056-8469-78d7d89ac61e', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:25:37', '2022-02-26 14:25:37'),
('be0a9db8-e26a-4674-92ca-86e4b2fe02fc', 1, NULL, NULL, NULL, NULL, '1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', 'c9fe9d07-5638-4145-9d6b-d5638d125a91', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:27:11', '2022-02-26 14:27:11'),
('c4d75b73-3003-4047-8c8c-63333c514499', 1, NULL, NULL, NULL, NULL, '12f5ab75-fd0b-4056-8469-78d7d89ac61e', '46f7dad7-126f-4929-ac6f-157645df774c', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('c6fec71f-f823-4875-9acd-61db6beb47cf', 4, NULL, NULL, NULL, NULL, '1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', '5a9d3b36-505b-4f80-8eec-baf1c4f4defc', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('d1a16999-41c7-4eb8-88b8-268d71047518', 1, NULL, NULL, NULL, NULL, '08a55159-dd55-4b67-91a1-a234c9ef95fc', '5a9d3b36-505b-4f80-8eec-baf1c4f4defc', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:57', '2022-02-26 14:26:57'),
('dbed1af6-3de2-4f9b-beb1-1bd03264444d', 1, NULL, NULL, NULL, NULL, '2286180d-8eb4-4567-bc4b-ddb88213e68c', '46f7dad7-126f-4929-ac6f-157645df774c', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('e2460b9f-4d5d-4fcf-9e66-983b966031f4', 3, NULL, NULL, NULL, NULL, '1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:25:37', '2022-02-26 14:25:37'),
('ef189473-682a-4d77-8041-751bc1db0f37', 1, NULL, NULL, NULL, NULL, '1b1c37d8-8ad2-49a3-873d-0c0d376bb6c9', '46f7dad7-126f-4929-ac6f-157645df774c', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:26:31', '2022-02-26 14:26:31'),
('f1a2e7de-7d5e-4223-9718-18716133e873', 2, NULL, NULL, NULL, NULL, '2286180d-8eb4-4567-bc4b-ddb88213e68c', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '450f0d60-03ed-4b89-a095-31ea9fb74385', NULL, 'active', '2022-02-26 14:25:37', '2022-02-26 14:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `related_documents`
--

CREATE TABLE `related_documents` (
  `id` varchar(255) NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `request_quotation_id` varchar(255) DEFAULT NULL,
  `terms_of_reference_id` varchar(255) DEFAULT NULL,
  `vendor_proposal_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_quotation`
--

CREATE TABLE `request_quotation` (
  `id` varchar(255) NOT NULL,
  `request_quotation_number` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `due_date` datetime NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `quotation_code` varchar(255) NOT NULL,
  `rfq_type` varchar(255) NOT NULL,
  `purchase_requisition_id` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `request_quotation`
--

INSERT INTO `request_quotation` (`id`, `request_quotation_number`, `message`, `status`, `due_date`, `prepared_by`, `quotation_code`, `rfq_type`, `purchase_requisition_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('8bbcce92-93ea-4dfd-9d34-c873f66acd1d', 8220, '<p>test</p>', 'On Going', '2022-03-12 00:00:00', 'Aime Arsolon', 'IT - 1127', 'procure_of_products', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', '425cc91d-e4df-4c80-9e28-2bcab395e36d', NULL, '2022-02-26 15:18:54', '2022-02-26 15:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `request_quotation_vendor`
--

CREATE TABLE `request_quotation_vendor` (
  `vendor_id` varchar(255) NOT NULL,
  `rfq_pr_id` varchar(255) DEFAULT NULL,
  `rfq_status` varchar(255) DEFAULT NULL,
  `approver_name` varchar(255) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `reject_reason` text DEFAULT NULL,
  `request_quotation_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `request_quotation_vendor`
--

INSERT INTO `request_quotation_vendor` (`vendor_id`, `rfq_pr_id`, `rfq_status`, `approver_name`, `approval_date`, `reject_reason`, `request_quotation_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('dd5374b1-2f45-43f5-8d61-a53774410025', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', 'Pending', NULL, NULL, NULL, '8bbcce92-93ea-4dfd-9d34-c873f66acd1d', NULL, NULL, '2022-02-26 15:18:54', '2022-02-26 15:18:54'),
('f625ca47-e96f-4f3c-b297-87cd56678b64', '4e9e2e27-af40-4fd7-b16d-442aac1557cd', 'Pending', NULL, NULL, NULL, '8bbcce92-93ea-4dfd-9d34-c873f66acd1d', NULL, NULL, '2022-02-26 15:18:54', '2022-02-26 15:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `returns`
--

CREATE TABLE `returns` (
  `id` varchar(255) NOT NULL,
  `return_date` date NOT NULL,
  `return_status` varchar(255) DEFAULT NULL,
  `return_type` varchar(255) NOT NULL,
  `returner` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `return_details`
--

CREATE TABLE `return_details` (
  `id` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `purchase_order_detail_id` varchar(255) DEFAULT NULL,
  `return_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `terms_of_reference`
--

CREATE TABLE `terms_of_reference` (
  `id` varchar(255) NOT NULL,
  `tor_number` int(11) DEFAULT NULL,
  `background` text DEFAULT NULL,
  `objective` text DEFAULT NULL,
  `scope_of_service` text DEFAULT NULL,
  `tor_deliverables` text DEFAULT NULL,
  `qualifications` text DEFAULT NULL,
  `reporting_and_working_arrangements` text DEFAULT NULL,
  `tor_annex_technical_specifications` text DEFAULT NULL,
  `tor_annex_key_experts` text DEFAULT NULL,
  `tor_annex_deliverables` text DEFAULT NULL,
  `tor_annex_terms_conditions` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `approver_name` varchar(255) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `project_request_id` varchar(255) DEFAULT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `terms_of_reference`
--

INSERT INTO `terms_of_reference` (`id`, `tor_number`, `background`, `objective`, `scope_of_service`, `tor_deliverables`, `qualifications`, `reporting_and_working_arrangements`, `tor_annex_technical_specifications`, `tor_annex_key_experts`, `tor_annex_deliverables`, `tor_annex_terms_conditions`, `status`, `prepared_by`, `approver_name`, `approval_date`, `reject_reason`, `project_request_id`, `vendor_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('51daae0a-25e7-4907-839a-0338a307c097', 2510, '<p>After the completion of the Retrofitting Works for the DOST-CO Building in\nFebruary 2020, it shall undergo renovation/ rehabilitation and expansion. With the\nmassive stripping of the building components (like interior finishes and utilities) to give\nway to the retrofitting works, the DOST-CO Building shall be renovated to suit the\nneeds of the employees and its operation. The renovation of the offices on all existing\nfloors shall include additional utilities, equipment and furniture. The design shall be\nresponsive to the current needs of the different offices which will also adapt a\nsustainable design. The design will also utilize the latest applicable Green Building\nTechnologies fitted to the DOST-CO Operations.&nbsp;<br></p>', '<p>A project objective describes the desired results of a project, which often includes a tangible\nitem. An objective is specific and measurable, and must meet time, budget, and quality\nconstraints.<br></p>', '<p>scope is the defined features and functions of a product, or the scope of work needed to\nfinish a project. Scope involves getting information required to start a project, including the\nfeatures the product needs to meet its stakeholders\' requirements&nbsp;<br></p>', '<p>This is associated to the proposed timeline of the project, more likely the proposed\nimplementation of each phases.</p><p><br></p><table class=\"table table-bordered\"><tbody><tr><td><b>Deliverables</b></td><td><b>Months</b></td></tr><tr><td>Inception Report<br></td><td>5</td></tr><tr><td>Human Resource Capacity Assessment\nReport&nbsp;<br></td><td>7</td></tr></tbody></table><p>&nbsp;<br></p>', '<p>The Consultancy Firm should have the following minimum qualifications:\n</p><p>1. Demonstrated expertise on social protection, business process assessment, digital\npayments, and MIS\n</p><p>2. Good standing and have been in the business for at least 10 years\n</p><p>3. With at least five (5) years of experience in providing process evaluation and/or training\nservices to any government agency</p><p>\n4. Have significant knowledge about DSWD and MSSD’s 4Ps and social protection\nprograms</p><p>\n5. Working experience and local presence in the BARMM is a plus.\n6. Should demonstrate that it has necessary human resources available to carry out task.\nQualifications of the key staff team are detailed in Annex B.&nbsp;<br></p>', '<p>The consulting firm will be supervised by DSWD’s 4Ps NPMO and will work closely with the\nMSSD-BARMM.<br></p>', '<p><span style=\"font-size: 1rem;\">&nbsp;</span><br></p>', '', '', '', 'Pending', 'Aime Arsolon', NULL, '2022-02-26', NULL, 'f8eee60b-0ef0-42de-82cd-5c63b047c64f', 'f625ca47-e96f-4f3c-b297-87cd56678b64', '425cc91d-e4df-4c80-9e28-2bcab395e36d', NULL, '2022-02-26 13:14:53', '2022-02-26 13:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_type_id` varchar(255) DEFAULT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `status`, `user_type_id`, `vendor_id`, `employee_id`, `created_at`, `updated_at`) VALUES
('33bbd0b9-852c-4567-b9c9-a803e0bf9344', 'test@gmail.com', '$2b$12$17igzVGpr5wHFhkde9/KEuHfSLj1szZkahjyZ1ULFOdJ7HYqC..ze', 'Active', '793a1816-92d6-4c44-a29b-b776d275492a', '60a23a4d-e40f-4f52-b065-40f5721807df', NULL, '2022-02-24 11:00:50', '2022-02-24 11:43:36'),
('41da7dff-841a-4289-8b35-5fd6fc14fa6d', 'ad@gmail.com', '$2b$12$v02yDgnznfqUluYsPhQVrOLFgZxS8pJDyxF9nq1A7HzlbkqGRNEuy', 'Active', '793a1816-92d6-4c44-a29b-b776d275492a', '489017b0-5c16-4c96-8fcc-02e31059640b', NULL, '2022-02-18 16:29:27', '2022-02-24 11:43:07'),
('425cc91d-e4df-4c80-9e28-2bcab395e36d', 'officer@gmail.com', '$2b$12$WH/oDcduUeazJDdz.MVyxeGcitbHpsAbaN1NKnR3KNslOBc0PZwMG', 'Active', 'e84d4414-e8b4-46cb-a713-89a0dab97753', NULL, '1bebbc91-4be4-46af-85ae-7b893d5ce271', '2022-02-17 22:24:18', '2022-02-24 11:44:15'),
('450f0d60-03ed-4b89-a095-31ea9fb74385', 'hr@gmail.com', '$2b$12$RklegkkMlROGusVSTer3.OJwg2Ggl2ruhFTtOmxxaMzQnQuscAugS', 'Active', 'e84d4414-e8b4-46cb-a713-89a0dab97753', NULL, 'ff8fd782-3331-43f3-a2e2-bf925c3eb3c5', '2022-02-17 22:25:44', '2022-02-24 11:43:48'),
('77a1df3b-f31a-40bb-bff5-a7ba9cafd1a7', 'manager@gmail.com', '$2b$12$7i9l0iPFSsn2K8seFopL8uIB0TzCNTK3K5hMFJnqN98aCTbwq16FG', 'Active', 'e84d4414-e8b4-46cb-a713-89a0dab97753', NULL, '0d82e69b-178e-4151-90f0-319c01aaed9f', '2022-02-17 22:23:34', '2022-02-24 11:43:27'),
('7ae815a9-0c33-4bc9-b1ed-3c356151ad32', 'azure@gmail.com', '$2b$12$LTAUlkdLVJ3hHyODzlT6VuMxKAlsdNBNkNIl3yJbhGLMWg9nVD23q', 'Active', '793a1816-92d6-4c44-a29b-b776d275492a', '55504072-0c3d-45fb-bf8e-5a1e33cccb47', NULL, '2022-02-19 19:11:07', '2022-02-24 11:43:20'),
('feecb94e-7744-4008-8ca8-2759e1c608f1', 'admin@gmail.com', '$2b$12$qd9ojau4viLjty9MBZBQPuo.beVIgdW/RqjtBK3V/eDAN4VKJNbhy', 'Active', 'e84d4414-e8b4-46cb-a713-89a0dab97753', NULL, '99e265a7-1049-4b4b-abe4-89b7caca3a95', '2022-02-18 11:07:13', '2022-02-24 11:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('793a1816-92d6-4c44-a29b-b776d275492a', 'outsider', 'string', 'Active', '2022-02-24 10:56:42', '2022-02-24 10:56:42'),
('e84d4414-e8b4-46cb-a713-89a0dab97753', 'insider', 'string', 'Active', '2022-02-17 22:04:13', '2022-02-22 10:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `utilities`
--

CREATE TABLE `utilities` (
  `id` varchar(255) NOT NULL,
  `utility_type` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `utility_amount` varchar(255) DEFAULT NULL,
  `due_date` varchar(255) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `utilities`
--

INSERT INTO `utilities` (`id`, `utility_type`, `attachment`, `utility_amount`, `due_date`, `notes`, `vendor_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('cf733620-b8fc-4a3e-9621-3e757e802f0a', 'Water Bill', 'Sample-Water-Bill.pdf', '20000', '2022-03-09', 'Sample Water Bill Notes', '86ea928b-90d0-4b73-a8ca-62bbc837bab1', 'pending', NULL, NULL, '2022-02-26 11:31:12', '2022-02-26 11:34:08');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` varchar(255) NOT NULL,
  `vendor_logo` varchar(255) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `vendor_website` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `organization_type` varchar(255) DEFAULT NULL,
  `region` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `municipality` varchar(255) DEFAULT NULL,
  `barangay` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `vendor_logo`, `vendor_name`, `contact_person`, `contact_no`, `vendor_website`, `email`, `organization_type`, `region`, `province`, `municipality`, `barangay`, `street`, `category_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('3c67dda4-2a2d-40e2-95e3-6581d074868b', 'jbm-logo.png', 'JBM', 'Denden Diaz', '091231231', 'JBM.com', 'jbm@gmail.com', 'n/a', 'NCR (National Capital Region)', 'First District', 'Choose Municipality', NULL, '123, street', 'bdbef0d7-4e29-4d1d-989f-e395941fec5c', 'active', NULL, NULL, '2022-02-26 10:50:07', '2022-02-26 10:50:07'),
('489017b0-5c16-4c96-8fcc-02e31059640b', 'download.png', 'Adtek', 'Kristan Diaz Balin', '923123', 'adtek.com', 'kristanbalin@gmail.com', 'n/a', 'NCR (National Capital Region)', 'Second District', 'Choose Municipality', NULL, '123', 'bdbef0d7-4e29-4d1d-989f-e395941fec5c', 'Active', NULL, NULL, '2022-02-18 13:21:00', '2022-02-26 10:21:37'),
('55504072-0c3d-45fb-bf8e-5a1e33cccb47', 'Microsoft_Azure.svg.png', 'Azure Company', 'Dani DOma', '0912312', 'website.com', 'azure@gmail.com', 'etest', 'Cagayan Valley (Region II)', 'Cagayan', 'Alcala', 'Afusing Daga', 'test', '43dc422a-8621-4966-8662-f8d53d5fdfc7', 'Active', NULL, NULL, '2022-02-19 15:54:17', '2022-02-22 22:01:59'),
('60a23a4d-e40f-4f52-b065-40f5721807df', '3m.png', '3m', 'merry ann', '923423', '3m.com', 'mcdo@gmail.com', 'sole proprietorship', 'NCR (National Capital Region)', 'First District', 'Choose Municipality', NULL, '21, street', '7479b7bf-2ae2-4815-bb80-7d9ba73820d7', 'active', NULL, NULL, '2022-02-24 10:05:34', '2022-02-26 11:09:03'),
('86ea928b-90d0-4b73-a8ca-62bbc837bab1', 'Maynilad_logo.jpg', 'Maynilad', 'MayMay NiLad', '09123412932', 'may.com', 'may@gmail.com', 'n/a', 'Central Luzon (Region III)', 'Pampanga', 'Arayat', 'Buensuceso', '123, street', '2f4fcd58-e1ee-479e-89b0-51fcd384c71e', 'active', NULL, NULL, '2022-02-26 11:16:02', '2022-02-26 11:16:02'),
('909a2ee6-09e7-4a0f-b6c2-87bb5667aca2', 'pms-logo-transparent-1024x165.png', 'Philippine Medical Supplies', 'Ochako San', '09123412932', 'pms.com', 'pms@gmail.com', 'n/a', 'NCR (National Capital Region)', 'Third District', 'Choose Municipality', NULL, 'street', 'd86dc4f6-10d1-4a94-b422-03d0a5310c3b', 'active', NULL, NULL, '2022-02-26 11:01:36', '2022-02-26 11:01:36'),
('94ee77a1-25ff-4bae-8177-7868f8914b6f', 'dentistree.png', 'dentistree', 'Ballesteros Aquino', '0912312312', 'dentree.com', 'dentree@gmail.com', 'n/a', 'NCR (National Capital Region)', 'Fourth District', 'Choose Municipality', NULL, 'street', 'f55f47b2-3247-48e8-805c-731694b9d342', 'active', NULL, NULL, '2022-02-26 10:54:30', '2022-02-26 10:54:30'),
('9e6748d0-73e5-4b8b-8186-6ad60bf12c45', 'builders.png', 'Builders PH', 'Dexter Machete', '9123123', 'builder.ph', 'builder@gmail.com', 'sole proprietorship', 'CALABARZON (Region IV-A)', 'Laguna', 'Choose Municipality', NULL, '123 street', 'cd434944-9ca7-4858-8c55-675ec1d40570', 'active', NULL, NULL, '2022-02-26 10:23:07', '2022-02-26 11:15:02'),
('dd5374b1-2f45-43f5-8d61-a53774410025', 'Dell_Logo.png', 'Dell', '0912312', '09123123', 'dell.com', 'dell@gmail.com', 'n/a', 'NCR (National Capital Region)', 'First District', 'Choose Municipality', NULL, 'street', '26a9a648-c08e-4117-9d01-7366ca8943d8', 'active', NULL, NULL, '2022-02-26 10:51:38', '2022-02-26 10:51:38'),
('f625ca47-e96f-4f3c-b297-87cd56678b64', 'ips-logo.png', 'IPSDECOR', 'Marley Bob', '0912314123', 'ips.com', 'ips@gmail.com', 'n/a', 'Central Luzon (Region III)', 'Bulacan', 'City of San Jose Del Monte', 'Francisco Homes-Mulawin', 'Tripple m', '3b50adf9-4040-491c-84da-0b4ae169a6cb', 'active', NULL, NULL, '2022-02-26 10:48:48', '2022-02-26 10:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_audit_trail`
--

CREATE TABLE `vendor_audit_trail` (
  `id` varchar(255) NOT NULL,
  `crud` varchar(255) NOT NULL,
  `client_ip` varchar(255) DEFAULT NULL,
  `table` varchar(255) NOT NULL,
  `payload` text DEFAULT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_audit_trail`
--

INSERT INTO `vendor_audit_trail` (`id`, `crud`, `client_ip`, `table`, `payload`, `vendor_id`, `created_at`) VALUES
('2376f33b-a10f-41c5-a01b-5fbda3bbd5da', 'Insert', '180.190.247.196', 'vendor_proposal', '{\"subtotal\":14884,\"notes\":\"\",\"discount\":2,\"tax\":1488.4,\"created_at\":\"2022-02-26\",\"status\":\"Pending\",\"total_amount\":16370,\"message\":\"<p>test</p>\",\"prepared_by\":\"aime arsolon\",\"contact_no\":\"09222222222\",\"arrival_date\":\"2022-02-26\",\"vendor_bidding_item\":[{\"product_name\":\"test\",\"category\":{\"category_name\":\"IT supplies\",\"description\":\"Any services, Software, Hardware or Third Party Support which we have agreed to supply to you in an Order and/or Statement of Work\",\"status\":\"Active\"},\"description\":\"<p>test</p>\",\"quantity\":122,\"price_per_unit\":122,\"status\":\"active\"}]}', '489017b0-5c16-4c96-8fcc-02e31059640b', '2022-02-26 12:21:13'),
('7de30454-3517-4949-8ac3-f7e7dd6fd7fd', 'Insert', '180.190.247.196', 'vendor_proposal', '{\"subtotal\":144,\"notes\":\"\",\"discount\":0,\"tax\":14.4,\"created_at\":\"2022-02-26\",\"status\":\"Pending\",\"total_amount\":158,\"message\":\"<p>test</p>\",\"prepared_by\":\"test\",\"contact_no\":\"123123\",\"arrival_date\":\"2022-03-05\",\"vendor_bidding_item\":[{\"product_name\":\"test\",\"category\":{\"category_name\":\"IT supplies\",\"description\":\"Any services, Software, Hardware or Third Party Support which we have agreed to supply to you in an Order and/or Statement of Work\",\"status\":\"Active\"},\"description\":\"<p>test</p>\",\"quantity\":12,\"price_per_unit\":12,\"status\":\"active\"}]}', '489017b0-5c16-4c96-8fcc-02e31059640b', '2022-02-26 12:31:05'),
('8f6192e0-a922-49f4-a0b4-100b99e1fb93', 'Update Status', '180.190.247.196', 'request_quotation_vendor', '{\"approver_name\":\"test\",\"reject_reason\":\"\",\"created_at\":\"2022-02-26T12:20:29\",\"rfq_status\":\"Approved\",\"approval_date\":\"2022-02-26\",\"updated_at\":\"2022-02-26T12:20:48\"}', '489017b0-5c16-4c96-8fcc-02e31059640b', '2022-02-26 12:20:48'),
('96c384e5-8397-4349-a42b-1dbb0d5f05d9', 'Insert', '222.127.185.217', 'vendor_proposal', '{\"subtotal\":149084,\"notes\":\"test\",\"discount\":0,\"tax\":14908.4,\"created_at\":\"2022-02-26\",\"status\":\"Pending\",\"total_amount\":163992,\"message\":\"<p>test</p>\",\"prepared_by\":\"Aime Arsolon\",\"contact_no\":\"0999999999\",\"arrival_date\":\"2022-02-26\",\"vendor_bidding_item\":[{\"product_name\":\"test\",\"category\":{\"category_name\":\"IT supplies\",\"description\":\"Any services, Software, Hardware or Third Party Support which we have agreed to supply to you in an Order and/or Statement of Work\",\"status\":\"Active\"},\"description\":\"<p>test</p>\",\"quantity\":122,\"price_per_unit\":1222,\"status\":\"active\"}]}', '489017b0-5c16-4c96-8fcc-02e31059640b', '2022-02-26 03:09:08'),
('aa5095df-51ed-456e-ab69-378ea727514d', 'Update Status', '222.127.185.217', 'request_quotation_vendor', '{\"approver_name\":\"test\",\"reject_reason\":\"\",\"created_at\":\"2022-02-26T03:03:13\",\"rfq_status\":\"Approved\",\"approval_date\":\"2022-02-26\",\"updated_at\":\"2022-02-26T03:05:35\"}', '489017b0-5c16-4c96-8fcc-02e31059640b', '2022-02-26 03:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_bidding_item`
--

CREATE TABLE `vendor_bidding_item` (
  `id` varchar(255) NOT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price_per_unit` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `vendor_proposal_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_blacklist`
--

CREATE TABLE `vendor_blacklist` (
  `id` varchar(255) NOT NULL,
  `vendor_id` varchar(255) DEFAULT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_performance_evaluation`
--

CREATE TABLE `vendor_performance_evaluation` (
  `id` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `timeliness` varchar(255) DEFAULT NULL,
  `reliability` varchar(255) DEFAULT NULL,
  `quality` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `reputation` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `purchase_order_id` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_proposal`
--

CREATE TABLE `vendor_proposal` (
  `id` varchar(255) NOT NULL,
  `proposal_number` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `total_amount` varchar(255) DEFAULT NULL,
  `prepared_by` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `is_ordered` tinyint(1) DEFAULT NULL,
  `request_quotation_id` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `awarded_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_time_log`
--

CREATE TABLE `vendor_time_log` (
  `id` varchar(255) NOT NULL,
  `logged_date` datetime NOT NULL,
  `logged_type` varchar(255) NOT NULL,
  `client_ip` varchar(255) DEFAULT NULL,
  `vendor_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_time_log`
--

INSERT INTO `vendor_time_log` (`id`, `logged_date`, `logged_type`, `client_ip`, `vendor_id`) VALUES
('0660b482-9787-48df-842a-17263aa26a94', '2022-02-26 13:06:29', 'Logged Out', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('2c1d07e3-6546-4f41-8a6e-4f1b7180c193', '2022-02-26 12:20:36', 'Logged In', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('502abc02-b238-46d0-8a76-46ae03791d36', '2022-02-26 13:42:05', 'Logged Out', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('758d7bca-f492-4d4b-9549-047e6ba88e7d', '2022-02-26 12:15:24', 'Logged In', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('7d9c2d02-f3ed-4cb2-8b78-9dc36efeacba', '2022-02-26 12:19:19', 'Logged In', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('c29f2d6f-823a-459f-bcbc-390dd34eefde', '2022-02-26 13:41:03', 'Logged In', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('cc4f06ea-c948-41ef-9f6d-dd9db7c7338a', '2022-02-26 03:04:39', 'Logged In', '222.127.185.217', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('ccc017c1-1756-4573-8d96-883ee25abb7f', '2022-02-26 12:18:24', 'Logged Out', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b'),
('d7b3d67f-41a1-4a6c-a84c-b2feb97dc9fc', '2022-02-26 12:19:49', 'Logged Out', '180.190.247.196', '489017b0-5c16-4c96-8fcc-02e31059640b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `budget_plan`
--
ALTER TABLE `budget_plan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_id` (`department_id`,`year`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_budget_plan_date_from` (`date_from`),
  ADD KEY `ix_budget_plan_date_to` (`date_to`),
  ADD KEY `ix_budget_plan_status` (`status`),
  ADD KEY `ix_budget_plan_given_budget` (`given_budget`),
  ADD KEY `ix_budget_plan_total_spent` (`total_spent`),
  ADD KEY `ix_budget_plan_year` (`year`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_category_description` (`description`(768)),
  ADD KEY `ix_category_category_name` (`category_name`),
  ADD KEY `ix_category_status` (`status`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_department_department_name` (`department_name`),
  ADD KEY `ix_department_contact_no` (`contact_no`),
  ADD KEY `ix_department_department_head` (`department_head`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_employees_birthdate` (`birthdate`),
  ADD KEY `ix_employees_first_name` (`first_name`),
  ADD KEY `ix_employees_contact_no` (`contact_no`),
  ADD KEY `ix_employees_last_name` (`last_name`),
  ADD KEY `ix_employees_status` (`status`),
  ADD KEY `ix_employees_middle_name` (`middle_name`),
  ADD KEY `employee_type_id` (`employee_type_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `employee_types`
--
ALTER TABLE `employee_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_employee_types_description` (`description`(768)),
  ADD KEY `ix_employee_types_name` (`name`),
  ADD KEY `ix_employee_types_status` (`status`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_invoice_amount_paid` (`amount_paid`),
  ADD KEY `ix_invoice_prepared_by` (`prepared_by`),
  ADD KEY `ix_invoice_message` (`message`(768)),
  ADD KEY `ix_invoice_status` (`status`),
  ADD KEY `ix_invoice_invoice_date` (`invoice_date`),
  ADD KEY `ix_invoice_due_date` (`due_date`),
  ADD KEY `ix_invoice_billing_address` (`billing_address`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `ix_notification_description` (`description`),
  ADD KEY `ix_notification_notif_to` (`notif_to`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_payment_method_description` (`description`);

--
-- Indexes for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_payment_terms_description` (`description`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `ix_product_description` (`description`(768)),
  ADD KEY `ix_product_status` (`status`),
  ADD KEY `ix_product_product_name` (`product_name`),
  ADD KEY `ix_product_estimated_price` (`estimated_price`);

--
-- Indexes for table `project_request`
--
ALTER TABLE `project_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_project_request_cost` (`cost`),
  ADD KEY `ix_project_request_coverage` (`coverage`(768)),
  ADD KEY `ix_project_request_constraints` (`constraints`),
  ADD KEY `ix_project_request_background` (`background`(768)),
  ADD KEY `ix_project_request_active_status` (`active_status`),
  ADD KEY `ix_project_request_name` (`name`),
  ADD KEY `ix_project_request_approval_status` (`approval_status`),
  ADD KEY `ix_project_request_expected_output` (`expected_output`(768)),
  ADD KEY `ix_project_request_end_date` (`end_date`),
  ADD KEY `ix_project_request_start_date` (`start_date`),
  ADD KEY `ix_project_request_objectives` (`objectives`(768)),
  ADD KEY `ix_project_request_target_beneficiaries` (`target_beneficiaries`),
  ADD KEY `ix_project_request_type` (`type`),
  ADD KEY `ix_project_request_assumptions` (`assumptions`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_proposal_id` (`vendor_proposal_id`),
  ADD UNIQUE KEY `ix_purchase_order_purchase_order_number` (`purchase_order_number`),
  ADD KEY `payment_terms_id` (`payment_terms_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_purchase_order_status` (`status`),
  ADD KEY `ix_purchase_order_order_date` (`order_date`),
  ADD KEY `ix_purchase_order_discount` (`discount`),
  ADD KEY `ix_purchase_order_tax` (`tax`),
  ADD KEY `ix_purchase_order_subtotal` (`subtotal`),
  ADD KEY `ix_purchase_order_total_amount` (`total_amount`),
  ADD KEY `ix_purchase_order_shipping_method` (`shipping_method`),
  ADD KEY `ix_purchase_order_notes` (`notes`),
  ADD KEY `ix_purchase_order_expected_delivery_date` (`expected_delivery_date`);

--
-- Indexes for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_purchase_order_detail_category` (`category`),
  ADD KEY `ix_purchase_order_detail_product_price` (`product_price`),
  ADD KEY `ix_purchase_order_detail_product_name` (`product_name`),
  ADD KEY `ix_purchase_order_detail_status` (`status`),
  ADD KEY `ix_purchase_order_detail_quantity` (`quantity`);

--
-- Indexes for table `purchase_requisition`
--
ALTER TABLE `purchase_requisition`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_purchase_requisition_purchase_requisition_number` (`purchase_requisition_number`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_purchase_requisition_purpose` (`purpose`),
  ADD KEY `ix_purchase_requisition_message` (`message`(768)),
  ADD KEY `ix_purchase_requisition_estimated_amount` (`estimated_amount`),
  ADD KEY `ix_purchase_requisition_reason` (`reason`),
  ADD KEY `ix_purchase_requisition_status` (`status`),
  ADD KEY `ix_purchase_requisition_date_approved` (`date_approved`),
  ADD KEY `ix_purchase_requisition_approved_by` (`approved_by`),
  ADD KEY `ix_purchase_requisition_given_budget` (`given_budget`);

--
-- Indexes for table `purchase_requisition_detail`
--
ALTER TABLE `purchase_requisition_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_purchase_requisition_detail_estimated_price` (`estimated_price`),
  ADD KEY `ix_purchase_requisition_detail_description` (`description`(768)),
  ADD KEY `ix_purchase_requisition_detail_status` (`status`),
  ADD KEY `ix_purchase_requisition_detail_new_category` (`new_category`),
  ADD KEY `ix_purchase_requisition_detail_new_product_name` (`new_product_name`),
  ADD KEY `ix_purchase_requisition_detail_quantity` (`quantity`);

--
-- Indexes for table `related_documents`
--
ALTER TABLE `related_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_quotation_id` (`request_quotation_id`),
  ADD KEY `terms_of_reference_id` (`terms_of_reference_id`),
  ADD KEY `vendor_proposal_id` (`vendor_proposal_id`),
  ADD KEY `ix_related_documents_attachment` (`attachment`),
  ADD KEY `ix_related_documents_status` (`status`);

--
-- Indexes for table `request_quotation`
--
ALTER TABLE `request_quotation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_request_quotation_request_quotation_number` (`request_quotation_number`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_request_quotation_quotation_code` (`quotation_code`),
  ADD KEY `ix_request_quotation_message` (`message`(768)),
  ADD KEY `ix_request_quotation_rfq_type` (`rfq_type`),
  ADD KEY `ix_request_quotation_due_date` (`due_date`),
  ADD KEY `ix_request_quotation_status` (`status`),
  ADD KEY `ix_request_quotation_prepared_by` (`prepared_by`);

--
-- Indexes for table `request_quotation_vendor`
--
ALTER TABLE `request_quotation_vendor`
  ADD PRIMARY KEY (`vendor_id`,`request_quotation_id`),
  ADD UNIQUE KEY `vendor_id` (`vendor_id`,`rfq_pr_id`),
  ADD KEY `rfq_pr_id` (`rfq_pr_id`),
  ADD KEY `request_quotation_id` (`request_quotation_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_request_quotation_vendor_rfq_status` (`rfq_status`),
  ADD KEY `ix_request_quotation_vendor_approval_date` (`approval_date`),
  ADD KEY `ix_request_quotation_vendor_reject_reason` (`reject_reason`(768)),
  ADD KEY `ix_request_quotation_vendor_approver_name` (`approver_name`);

--
-- Indexes for table `returns`
--
ALTER TABLE `returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_returns_return_type` (`return_type`),
  ADD KEY `ix_returns_return_status` (`return_status`),
  ADD KEY `ix_returns_returner` (`returner`);

--
-- Indexes for table `return_details`
--
ALTER TABLE `return_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_detail_id` (`purchase_order_detail_id`),
  ADD KEY `return_id` (`return_id`),
  ADD KEY `ix_return_details_status` (`status`);

--
-- Indexes for table `terms_of_reference`
--
ALTER TABLE `terms_of_reference`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_terms_of_reference_tor_number` (`tor_number`),
  ADD KEY `project_request_id` (`project_request_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_terms_of_reference_tor_annex_technical_specifications` (`tor_annex_technical_specifications`(768)),
  ADD KEY `ix_terms_of_reference_reject_reason` (`reject_reason`),
  ADD KEY `ix_terms_of_reference_objective` (`objective`(768)),
  ADD KEY `ix_terms_of_reference_tor_annex_deliverables` (`tor_annex_deliverables`(768)),
  ADD KEY `ix_terms_of_reference_tor_deliverables` (`tor_deliverables`(768)),
  ADD KEY `ix_terms_of_reference_scope_of_service` (`scope_of_service`(768)),
  ADD KEY `ix_terms_of_reference_tor_annex_terms_conditions` (`tor_annex_terms_conditions`(768)),
  ADD KEY `ix_terms_of_reference_prepared_by` (`prepared_by`),
  ADD KEY `ix_terms_of_reference_status` (`status`),
  ADD KEY `ix_terms_of_reference_qualifications` (`qualifications`(768)),
  ADD KEY `ix_terms_of_reference_approver_name` (`approver_name`),
  ADD KEY `ix_terms_of_reference_background` (`background`(768)),
  ADD KEY `ix_terms_of_reference_reporting_and_working_arrangements` (`reporting_and_working_arrangements`(768)),
  ADD KEY `ix_terms_of_reference_tor_annex_key_experts` (`tor_annex_key_experts`(768)),
  ADD KEY `ix_terms_of_reference_approval_date` (`approval_date`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_users_email` (`email`),
  ADD KEY `ix_users_status` (`status`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_types_name` (`name`),
  ADD KEY `ix_user_types_status` (`status`),
  ADD KEY `ix_user_types_updated_by` (`updated_by`),
  ADD KEY `ix_user_types_created_by` (`created_by`),
  ADD KEY `ix_user_types_description` (`description`(768));

--
-- Indexes for table `utilities`
--
ALTER TABLE `utilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_utilities_utility_type` (`utility_type`),
  ADD KEY `ix_utilities_status` (`status`),
  ADD KEY `ix_utilities_attachment` (`attachment`),
  ADD KEY `ix_utilities_utility_amount` (`utility_amount`),
  ADD KEY `ix_utilities_due_date` (`due_date`),
  ADD KEY `ix_utilities_notes` (`notes`(768));

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_vendor_vendor_logo` (`vendor_logo`),
  ADD UNIQUE KEY `ix_vendor_email` (`email`),
  ADD UNIQUE KEY `ix_vendor_vendor_name` (`vendor_name`),
  ADD KEY `ix_vendor_status` (`status`),
  ADD KEY `ix_vendor_contact_no` (`contact_no`),
  ADD KEY `ix_vendor_vendor_website` (`vendor_website`),
  ADD KEY `ix_vendor_organization_type` (`organization_type`),
  ADD KEY `ix_vendor_region` (`region`),
  ADD KEY `ix_vendor_province` (`province`),
  ADD KEY `ix_vendor_contact_person` (`contact_person`),
  ADD KEY `ix_vendor_municipality` (`municipality`),
  ADD KEY `ix_vendor_barangay` (`barangay`),
  ADD KEY `ix_vendor_street` (`street`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `vendor_audit_trail`
--
ALTER TABLE `vendor_audit_trail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `ix_vendor_audit_trail_crud` (`crud`),
  ADD KEY `ix_vendor_audit_trail_table` (`table`),
  ADD KEY `ix_vendor_audit_trail_payload` (`payload`(768)),
  ADD KEY `ix_vendor_audit_trail_client_ip` (`client_ip`);

--
-- Indexes for table `vendor_bidding_item`
--
ALTER TABLE `vendor_bidding_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `vendor_proposal_id` (`vendor_proposal_id`),
  ADD KEY `ix_vendor_bidding_item_quantity` (`quantity`),
  ADD KEY `ix_vendor_bidding_item_status` (`status`),
  ADD KEY `ix_vendor_bidding_item_price_per_unit` (`price_per_unit`),
  ADD KEY `ix_vendor_bidding_item_product_name` (`product_name`),
  ADD KEY `ix_vendor_bidding_item_description` (`description`(768));

--
-- Indexes for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_vendor_blacklist_vendor_name` (`vendor_name`),
  ADD KEY `ix_vendor_blacklist_email` (`email`),
  ADD KEY `ix_vendor_blacklist_status` (`status`),
  ADD KEY `ix_vendor_blacklist_remarks` (`remarks`(768));

--
-- Indexes for table `vendor_performance_evaluation`
--
ALTER TABLE `vendor_performance_evaluation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_vendor_performance_evaluation_status` (`status`),
  ADD KEY `ix_vendor_performance_evaluation_timeliness` (`timeliness`),
  ADD KEY `ix_vendor_performance_evaluation_reliability` (`reliability`),
  ADD KEY `ix_vendor_performance_evaluation_message` (`message`(768)),
  ADD KEY `ix_vendor_performance_evaluation_quality` (`quality`),
  ADD KEY `ix_vendor_performance_evaluation_cost` (`cost`),
  ADD KEY `ix_vendor_performance_evaluation_availability` (`availability`),
  ADD KEY `ix_vendor_performance_evaluation_reputation` (`reputation`);

--
-- Indexes for table `vendor_proposal`
--
ALTER TABLE `vendor_proposal`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `created_by` (`created_by`,`request_quotation_id`),
  ADD UNIQUE KEY `ix_vendor_proposal_proposal_number` (`proposal_number`),
  ADD KEY `request_quotation_id` (`request_quotation_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_vendor_proposal_arrival_date` (`arrival_date`),
  ADD KEY `ix_vendor_proposal_is_ordered` (`is_ordered`),
  ADD KEY `ix_vendor_proposal_tax` (`tax`),
  ADD KEY `ix_vendor_proposal_discount` (`discount`),
  ADD KEY `ix_vendor_proposal_awarded_by` (`awarded_by`),
  ADD KEY `ix_vendor_proposal_total_amount` (`total_amount`),
  ADD KEY `ix_vendor_proposal_prepared_by` (`prepared_by`),
  ADD KEY `ix_vendor_proposal_contact_no` (`contact_no`),
  ADD KEY `ix_vendor_proposal_subtotal` (`subtotal`),
  ADD KEY `ix_vendor_proposal_message` (`message`(768)),
  ADD KEY `ix_vendor_proposal_notes` (`notes`(768)),
  ADD KEY `ix_vendor_proposal_status` (`status`);

--
-- Indexes for table `vendor_time_log`
--
ALTER TABLE `vendor_time_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `ix_vendor_time_log_logged_type` (`logged_type`),
  ADD KEY `ix_vendor_time_log_logged_date` (`logged_date`),
  ADD KEY `ix_vendor_time_log_client_ip` (`client_ip`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `budget_plan`
--
ALTER TABLE `budget_plan`
  ADD CONSTRAINT `budget_plan_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_plan_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_plan_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `category_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `department_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`employee_type_id`) REFERENCES `employee_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD CONSTRAINT `payment_method_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_method_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD CONSTRAINT `payment_terms_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_terms_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `project_request`
--
ALTER TABLE `project_request`
  ADD CONSTRAINT `project_request_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `project_request_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `purchase_order_ibfk_1` FOREIGN KEY (`payment_terms_id`) REFERENCES `payment_terms` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_2` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_3` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_4` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_ibfk_6` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD CONSTRAINT `purchase_order_detail_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_detail_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_detail_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `purchase_requisition`
--
ALTER TABLE `purchase_requisition`
  ADD CONSTRAINT `purchase_requisition_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_requisition_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_requisition_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `purchase_requisition_detail`
--
ALTER TABLE `purchase_requisition_detail`
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_2` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `related_documents`
--
ALTER TABLE `related_documents`
  ADD CONSTRAINT `related_documents_ibfk_1` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `related_documents_ibfk_2` FOREIGN KEY (`terms_of_reference_id`) REFERENCES `terms_of_reference` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `related_documents_ibfk_3` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `request_quotation`
--
ALTER TABLE `request_quotation`
  ADD CONSTRAINT `request_quotation_ibfk_1` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_quotation_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_quotation_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `request_quotation_vendor`
--
ALTER TABLE `request_quotation_vendor`
  ADD CONSTRAINT `request_quotation_vendor_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_2` FOREIGN KEY (`rfq_pr_id`) REFERENCES `request_quotation` (`purchase_requisition_id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_3` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `request_quotation_vendor_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `returns`
--
ALTER TABLE `returns`
  ADD CONSTRAINT `returns_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `returns_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `return_details`
--
ALTER TABLE `return_details`
  ADD CONSTRAINT `return_details_ibfk_1` FOREIGN KEY (`purchase_order_detail_id`) REFERENCES `purchase_order_detail` (`id`),
  ADD CONSTRAINT `return_details_ibfk_2` FOREIGN KEY (`return_id`) REFERENCES `returns` (`id`);

--
-- Constraints for table `terms_of_reference`
--
ALTER TABLE `terms_of_reference`
  ADD CONSTRAINT `terms_of_reference_ibfk_1` FOREIGN KEY (`project_request_id`) REFERENCES `project_request` (`id`),
  ADD CONSTRAINT `terms_of_reference_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `terms_of_reference_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `terms_of_reference_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `utilities`
--
ALTER TABLE `utilities`
  ADD CONSTRAINT `utilities_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `utilities_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `utilities_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `vendor_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_audit_trail`
--
ALTER TABLE `vendor_audit_trail`
  ADD CONSTRAINT `vendor_audit_trail_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_bidding_item`
--
ALTER TABLE `vendor_bidding_item`
  ADD CONSTRAINT `vendor_bidding_item_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_bidding_item_ibfk_2` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
  ADD CONSTRAINT `vendor_blacklist_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_blacklist_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_blacklist_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_performance_evaluation`
--
ALTER TABLE `vendor_performance_evaluation`
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_3` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_proposal`
--
ALTER TABLE `vendor_proposal`
  ADD CONSTRAINT `vendor_proposal_ibfk_1` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_proposal_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vendor_proposal_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `vendor_time_log`
--
ALTER TABLE `vendor_time_log`
  ADD CONSTRAINT `vendor_time_log_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
