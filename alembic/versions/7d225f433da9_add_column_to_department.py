"""add column to department

Revision ID: 7d225f433da9
Revises: 5f2b9bd5e39c
Create Date: 2021-09-11 11:11:16.521597

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7d225f433da9'
down_revision = '5f2b9bd5e39c'
branch_labels = None
depends_on = None


def upgrade():
    pass
    #     op.add_column(
    #     'department',
    #     sa.Column('department_head',sa.String(200)),
    # )


def downgrade():
    pass
