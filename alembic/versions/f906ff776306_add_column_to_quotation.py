"""add column to quotation

Revision ID: f906ff776306
Revises: 575800c1daf5
Create Date: 2021-09-07 18:55:52.652375

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f906ff776306'
down_revision = '575800c1daf5'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # op.add_column(
    #     'request_quotation',
    #     sa.Column('po_status',sa.String(255)),
    # )


def downgrade():
    pass
