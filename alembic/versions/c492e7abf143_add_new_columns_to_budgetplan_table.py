"""add new columns to budgetplan table

Revision ID: c492e7abf143
Revises: 790cd8c6053b
Create Date: 2021-08-24 20:05:28.794169

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c492e7abf143'
down_revision = '790cd8c6053b'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # op.add_column(
    #     'budget_plan',
    #     # sa.Column('quarter',sa.String(255)),
    #     # sa.Column('plan_date_from',sa.DateTime),
    #     # sa.Column('plan_date_to',sa.DateTime),
    # )
    # op.drop_column('budget_plan', 'budget_plan_status')


def downgrade():
    pass
