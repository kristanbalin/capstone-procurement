"""Add a column

Revision ID: 9568e1c3d7b1
Revises: cb1babdb841f
Create Date: 2021-12-03 11:43:46.015574

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9568e1c3d7b1'
down_revision = 'cb1babdb841f'
branch_labels = None
depends_on = None


def upgrade():
    # pass
    op.add_column('product', sa.Column('estimated_price', sa.DECIMAL))


def downgrade():
    pass
