"""add status column to pr table

Revision ID: 790cd8c6053b
Revises: 3821418dd745
Create Date: 2021-08-24 10:38:26.474472

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '790cd8c6053b'
down_revision = '3821418dd745'
branch_labels = None
depends_on = None


def upgrade():
    pass
    #     op.add_column(
    #     'purchase_requesition',
    #     sa.Column('Status',sa.String(200))
    # )


def downgrade():
        op.drop_column('purchase_requesition', 'String')

