"""Add a column

Revision ID: cb1babdb841f
Revises: 6bdc49696edc
Create Date: 2021-11-18 15:52:49.558012

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cb1babdb841f'
down_revision = '6bdc49696edc'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
