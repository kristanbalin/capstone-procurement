"""add column to vendor table

Revision ID: 8c3a38e8e225
Revises: 056b264079a4
Create Date: 2022-01-27 13:21:42.660294

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8c3a38e8e225'
down_revision = '056b264079a4'
branch_labels = None
depends_on = None


def upgrade():
        op.add_column(
        'vendor',
        sa.Column('notifications',sa.Integer, nullable=True,default=0)
    )
    # pass


def downgrade():
    pass
