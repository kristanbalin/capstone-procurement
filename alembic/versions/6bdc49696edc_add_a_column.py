"""Add a column

Revision ID: 6bdc49696edc
Revises: 7d225f433da9
Create Date: 2021-11-13 16:17:37.613358

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6bdc49696edc'
down_revision = '7d225f433da9'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # op.add_column('vendor_proposal', sa.Column('delivery_days', sa.String(200)))


def downgrade():
    pass
