"""Add a column to pr detail

Revision ID: 6c2415c66921
Revises: 9568e1c3d7b1
Create Date: 2021-12-03 12:22:03.868565

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6c2415c66921'
down_revision = '9568e1c3d7b1'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # op.add_column('purchase_requisition_detail', sa.Column('estimated_price', sa.DECIMAL))



def downgrade():
    pass
