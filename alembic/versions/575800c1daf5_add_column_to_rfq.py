"""add column to rfq

Revision ID: 575800c1daf5
Revises: 48022ce65366
Create Date: 2021-08-31 20:04:56.081322

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '575800c1daf5'
down_revision = '48022ce65366'
branch_labels = None
depends_on = None


def upgrade():
    pass
    # op.add_column(
    #     'request_quotation',
    #     sa.Column('prepared_by',sa.String(200)),
    #     sa.Column('status',sa.String(200)),
    #     sa.Column('date',sa.DateTime),
    # )


def downgrade():
    pass
